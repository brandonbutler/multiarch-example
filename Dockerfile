FROM golang:alpine AS builder
COPY . /go/build
WORKDIR /go/build
ARG GOOS
ARG GOARCH
RUN echo "GOOS=$GOOS and GOARCH=$GOARCH" && \
    GOOS=$GOOS GOARCH=$GOARCH go build -o /GOBINARY

FROM alpine:latest
COPY --from=builder /GOBINARY /GOBINARY
CMD /GOBINARY
